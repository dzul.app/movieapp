import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import Color from '../common/Color';

const Footer = props => {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <Text style={styles.titleFooter}>{props.title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 50,
    backgroundColor: Color.primary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleFooter: {
    color: Color.white,
  },
});
export default Footer;
