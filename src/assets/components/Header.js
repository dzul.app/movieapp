import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Color from '../common/Color';

const Header = props => {
  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginHorizontal: 15,
        }}>
        <Text style={styles.titleHeader}>{props.title}</Text>
        <View>
          {props.isShow ? <Text style={styles.titleHeader2}>Hai,</Text> : null}
          {/* <Text style={styles.titleHeader2}>Hai,</Text> */}
          <Text style={styles.titleHeader2}>{props.title2}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 100,
    justifyContent: 'flex-end',
    backgroundColor: Color.primary,
    padding: 14,
  },
  titleHeader: {
    color: Color.white,
    fontSize: 20,
  },
  titleHeader2: {
    alignSelf: 'center',
    color: Color.white,
    fontSize: 12,
  },
});
export default Header;
