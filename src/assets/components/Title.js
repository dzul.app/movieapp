import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Color from '../common/Color';

const Title = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{props.title}</Text>
      <View style={styles.line} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
  },
  title: {
    marginHorizontal: 10,
    fontSize: 20,
    color: Color.black,
  },
  line: {
    borderBottomWidth: 1,
    borderBottomColor: Color.black,
    width: '95%',
    alignSelf: 'center',
  },
});
export default Title;
