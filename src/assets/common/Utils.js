const capitalizeFirstLetter = param => {
  const type = String(param);
  if (param !== 0) {
    return type.charAt(0).toUpperCase() + type.slice(1);
  } else {
    return type;
  }
};

export default capitalizeFirstLetter;
