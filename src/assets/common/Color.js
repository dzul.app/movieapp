export default {
  primary: '#F1C40F',
  accent: '#FFEAA7',
  black: '#000000',
  white: '#FFFFFF',
  grey: '#7F98B5',
};
