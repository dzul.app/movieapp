import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import Header from '../../assets/components/Header';
import Footer from '../../assets/components/Footer';
import Title from '../../assets/components/Title';
import Color from '../../assets/common/Color';
import {useSelector} from 'react-redux';

const Home = ({navigation, route}) => {
  const globalState = useSelector(state => state);
  const [data, setData] = useState();

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    let url = 'http://www.omdbapi.com/?apikey=966785a7&s=avenger';
    fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(res => {
        setData(res.Search);
      });
  };

  return (
    <View style={styles.container}>
      <Header title="Home" isShow={true} title2={globalState.name} />
      <View style={styles.wrapContent}>
        <Title title="Movie List" />
        <FlatList
          data={data}
          keyExtractor={item => item.imdbID}
          renderItem={({item}) => (
            <TouchableOpacity
              style={styles.wrapItem}
              onPress={() =>
                navigation.navigate('Detail', {
                  id: item.imdbID,
                  title: item.Title,
                  image: item.Poster,
                  year: item.Year,
                  type: item.Type,
                })
              }>
              <RenderItem item={item} />
            </TouchableOpacity>
          )}
        />
      </View>
      <Footer
        title="About Us"
        onPress={() =>
          navigation.navigate('About', {
            title: 'About Screen',
          })
        }
      />
    </View>
  );
};

const RenderItem = ({item}) => {
  return (
    <View>
      <View style={{flexDirection: 'row'}}>
        <Image
          source={{uri: item.Poster}}
          style={{margin: 5, height: 125, width: 80, borderRadius: 10}}
        />
        <View style={{flex: 1, padding: 5}}>
          <Text numberOfLines={1} style={{fontSize: 18, fontWeight: 'bold'}}>
            {item.Title}
          </Text>
          <Text style={{fontSize: 15}}>Year: {item.Year}</Text>
          <Text style={{fontSize: 15}}>Type: {item.Type.toUpperCase()}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.white,
  },
  wrapContent: {
    flex: 1,
    backgroundColor: 'white',
  },
  wrapItem: {
    borderRadius: 10,
    flex: 1,
    backgroundColor: 'white',
    borderColor: 'purple',
    borderWidth: 0.5,
    marginHorizontal: 10,
    marginVertical: 10,
  },
});

export default Home;
