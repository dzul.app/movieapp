import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useSelector, useDispatch} from 'react-redux';
import Color from '../../assets/common/Color';
import Header from '../../assets/components/Header';

const Login = props => {
  const LoginReducer = useSelector(state => state.LoginReducer);
  const dispatch = useDispatch();
  // const [form, setForm] = useState({
  //   username: '',
  //   password: '',
  // });
  const [isError, setIsError] = useState(false);

  const inputChangeHandler = (input, value) => {
    dispatch({type: 'SET_FORM', inputType: input, inputValue: value});
    // setForm({
    //   ...form,
    //   [input]: value,
    // });
  };

  const loginHandler = () => {
    if (
      LoginReducer.form.username === 'admin' &&
      LoginReducer.form.password === 'admin'
    ) {
      props.navigation.navigate('Home', {
        username: LoginReducer.form.username,
      });
    } else {
      setIsError(true);
    }
  };

  return (
    <View style={styles.container}>
      {/* <View style={styles.navBar}>
        <Text style={styles.title}>Login</Text>
      </View> */}
      <Header title="Login" />
      <View style={styles.body}>
        <View style={styles.logoContainer}>
          <View style={styles.logo}>
            <Text style={styles.logoTitle}>Movie </Text>
            <Text style={styles.logoTitle}>App</Text>
          </View>
        </View>
        <View style={styles.inputContainer}>
          <View style={styles.wrapContentInput}>
            <Icon
              name="email"
              size={20}
              color={Color.primary}
              style={styles.wrapIcon}
            />
            <TextInput
              placeholder="Username"
              autoCapitalize="none"
              style={styles.contentInput}
              value={LoginReducer.form.username}
              onChangeText={value => inputChangeHandler('username', value)}
            />
          </View>
          <View style={{height: 20}} />
          <View style={styles.wrapContentInput}>
            <Icon
              name="lock"
              size={20}
              color={Color.primary}
              style={styles.wrapIcon}
            />
            <TextInput
              placeholder="Password"
              secureTextEntry={true}
              style={styles.contentInput}
              value={LoginReducer.form.password}
              onChangeText={value => inputChangeHandler('password', value)}
            />
          </View>
          <View style={{height: 10}} />
          {isError ? (
            <Text style={styles.wrapContentError}>
              Your username or password Login wrong.
            </Text>
          ) : (
            <Text />
          )}
          <TouchableOpacity style={styles.button} onPress={loginHandler}>
            <Text style={styles.buttonText}>Login</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.tabNav}>
        <Text style={styles.textTabNav}>Terms & Conditions</Text>
        <Text style={styles.textTabNav}>Copyright ©</Text>
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navBar: {
    width: '100%',
    height: 100,
    backgroundColor: Color.primary,
    justifyContent: 'flex-end',
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'white',
    margin: 15,
  },
  body: {
    flex: 1,
    justifyContent: 'space-between',
  },
  logoContainer: {
    width: '100%',
    height: 200,
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: Color.accent,
  },
  logo: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderColor: Color.primary,
    width: 125,
    height: 125,
    backgroundColor: 'white',
    borderRadius: 100,
    borderWidth: 2,
  },
  logoTitle: {
    fontWeight: 'bold',
    fontSize: 25,
  },
  inputContainer: {
    flex: 1,
    width: '100%',
    height: 300,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  button: {
    width: '85%',
    height: 55,
    borderRadius: 15,
    backgroundColor: Color.primary,
    alignSelf: 'center',
    justifyContent: 'center',
    marginVertical: 20,
  },
  buttonText: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 20,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  tabNav: {
    justifyContent: 'center',
    width: '100%',
    height: 50,
    backgroundColor: Color.primary,
  },
  textTabNav: {
    alignSelf: 'center',
    fontSize: 14,
    color: 'grey',
  },
  wrapIcon: {
    alignSelf: 'center',
    marginRight: 10,
  },
  wrapContentError: {
    color: 'red',
    fontStyle: 'italic',
    fontSize: 11,
    alignSelf: 'center',
  },
  wrapContentInput: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  contentInput: {
    borderBottomColor: Color.primary,
    borderBottomWidth: 2,
    width: '80%',
    alignSelf: 'center',
  },
});
