import React, {useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Splash = props => {
  useEffect(() => {
    setTimeout(() => {
      props.navigation.navigate('Login');
    }, 2000);
  });
  return (
    <View style={styles.container}>
      <View style={styles.logoContainer}>
        <Text style={styles.logoTitle}>Movie </Text>
        <Text style={styles.logoTitle}>App</Text>
      </View>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffeaa7',
    justifyContent: 'center',
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: 175,
    height: 175,
    backgroundColor: 'white',
    borderRadius: 100,
    borderWidth: 2,
  },
  logoTitle: {
    fontWeight: 'bold',
    fontSize: 30,
  },
});
