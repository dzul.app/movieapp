import React from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Color from '../../assets/common/Color';
import Footer from '../../assets/components/Footer';
import Header from '../../assets/components/Header';

const About = ({route}) => {
  const {title} = route.params;
  return (
    <View style={styles.container}>
      <Header title={title} />
      <View style={styles.wrapContent}>
        <View style={styles.wrapItem}>
          <Image
            style={styles.imageUs}
            source={require('../../assets/Img/Akmal.jpg')}
          />
          <View style={styles.wrapIdentity}>
            <View style={styles.wrapContentIdentity}>
              <Icon
                name="account"
                size={18}
                color={Color.primary}
                style={styles.wrapContentIcon}
              />
              <Text>Akmal Maula Rasyid</Text>
            </View>
            <View style={styles.wrapContentIdentity}>
              <Icon
                name="telegram"
                size={18}
                color={Color.primary}
                style={styles.wrapContentIcon}
              />
              <Text>akmalmrsyd</Text>
            </View>
            <View style={styles.wrapContentIdentity}>
              <Icon
                name="email"
                size={18}
                color={Color.primary}
                style={styles.wrapContentIcon}
              />
              <Text>akmallmr@gmail.com</Text>
            </View>
            <View style={styles.wrapContentIdentity}>
              <Icon
                name="feather"
                size={18}
                color={Color.primary}
                style={styles.wrapContentIcon}
              />
              <Text>Hobby: Photography</Text>
            </View>
            <View style={styles.wrapContentIdentity}>
              <Icon
                name="react"
                size={18}
                color={Color.primary}
                style={styles.wrapContentIcon}
              />
              <Text>Interested in: Mobile Developer</Text>
            </View>
            <Text>{'    '}</Text>
            <View style={styles.wrapContentIdentity}>
              <Icon
                name="play"
                size={18}
                color={Color.primary}
                style={styles.wrapContentIcon}
              />
              <Text>Whats next to learn programming language is Pyhton</Text>
            </View>
          </View>
        </View>
        <View style={styles.wrapItem}>
          <Image
            style={styles.imageUs}
            source={require('../../assets/Img/Hilmy.jpg')}
          />
          <View style={styles.wrapIdentity}>
            <View style={styles.wrapContentIdentity}>
              <Icon
                name="account"
                size={18}
                color={Color.primary}
                style={styles.wrapContentIcon}
              />
              <Text>Hilmy Dzul Faqar</Text>
            </View>
            <View style={styles.wrapContentIdentity}>
              <Icon
                name="telegram"
                size={18}
                color={Color.primary}
                style={styles.wrapContentIcon}
              />
              <Text>dzul_faqar</Text>
            </View>
            <View style={styles.wrapContentIdentity}>
              <Icon
                name="email"
                size={18}
                color={Color.primary}
                style={styles.wrapContentIcon}
              />
              <Text>hdzulfaqar@gmail.com</Text>
            </View>
            <View style={styles.wrapContentIdentity}>
              <Icon
                name="feather"
                size={18}
                color={Color.primary}
                style={styles.wrapContentIcon}
              />
              <Text>Hobby: Music, Watch Movie</Text>
            </View>
            <View style={styles.wrapContentIdentity}>
              <Icon
                name="react"
                size={18}
                color={Color.primary}
                style={styles.wrapContentIcon}
              />
              <Text>
                Interested in: Backend Programming & Mobile Programming
              </Text>
            </View>
            <Text>{'    '}</Text>
            <View style={styles.wrapContentIdentity}>
              <Icon
                name="play"
                size={18}
                color={Color.primary}
                style={styles.wrapContentIcon}
              />
            </View>
          </View>
        </View>
      </View>
      <Footer />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.white,
  },
  wrapContent: {
    flex: 1,
    backgroundColor: 'white',
  },
  wrapItem: {
    height: '50%',
    padding: 10,
    flexDirection: 'row',
  },
  imageUs: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  wrapIdentity: {
    marginLeft: 10,
    flex: 1,
  },
  wrapContentIdentity: {
    flexDirection: 'row',
  },
  wrapContentIcon: {
    alignSelf: 'center',
    marginRight: 5,
  },
});
export default About;
