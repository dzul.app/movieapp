import React, {useState} from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Splash from './Splash/Splash';
import Login from './Login/Login';
import MainNavigator from './MainNavigator';

const Stack = createStackNavigator();

const route = () => {
  return (
    <NavigationContainer>
      {/* <MainNavigator /> */}
      <Stack.Navigator screenOptions={{headerShown: null}}>
        <Stack.Screen
          name="Splash"
          component={Splash}
          options={{title: 'Splash'}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{title: 'Login'}}
        />
        <Stack.Screen
          name="Home"
          component={MainNavigator}
          options={{headerShown: null}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default route;
