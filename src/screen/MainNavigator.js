import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Home from './Home/Home';
import Detail from './Detail/Detail';
import About from './About/About';

const Stack = createStackNavigator();
const MainNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={Home} options={{title: 'Home'}} />
      <Stack.Screen
        name="Detail"
        component={Detail}
        options={{title: 'Detail'}}
      />
      <Stack.Screen name="About" component={About} options={{title: 'About'}} />
    </Stack.Navigator>
  );
};

export default MainNavigator;
