import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import Header from '../../assets/components/Header';
import Footer from '../../assets/components/Footer';
import capitalizeFirstLetter from '../../assets/common/Utils';
import Axios from 'axios';
import Title from '../../assets/components/Title';

const Detail = ({route}) => {
  const [data, setData] = useState({});
  const {id} = route.params;

  useEffect(() => {
    getData(id);
  }, [id]);

  const getData = id => {
    Axios.get('http://www.omdbapi.com/', {
      params: {
        apikey: '966785a7',
        i: id,
      },
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        setData(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  };

  return (
    <View style={styles.container}>
      <Header title={data.Title} />
      <View style={styles.body}>
        <View style={styles.wrapContent}>
          <Image source={{uri: data.Poster}} style={styles.contentImage} />
          <View style={styles.wrapContentIdentity}>
            <Text>Year: {data.Year}</Text>
            <Text>Type: {capitalizeFirstLetter(data.Type)}</Text>
            <Text>Imdb Rating: {data.imdbRating}</Text>
            <Text>Metascore: {data.Metascore}</Text>
            <Text>Actors: {data.Actors}</Text>
            <Text>Director: {data.Director}</Text>
          </View>
        </View>
        <View>
          <Title title="Description" />
          <Text style={styles.wrapContentBody}>{data.Plot}</Text>
        </View>
        <View>
          <Title title="Comments" />
          <Text style={styles.wrapContentBody}>Here's your comments</Text>
        </View>
      </View>
      <Footer />
    </View>
  );
};

export default Detail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body: {
    flex: 1,
  },
  wrapContent: {
    flexDirection: 'row',
  },
  contentImage: {
    height: 225,
    width: 140,
    margin: 10,
    borderRadius: 5,
  },
  wrapContentIdentity: {
    flex: 1,
    padding: 10,
  },
  wrapContentBody: {
    marginLeft: 10,
    fontSize: 15,
    padding: 10,
  },
});
