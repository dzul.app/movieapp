import {combineReducers} from 'redux';

const initialState = {
  name: 'admin',
};

const initialStateLogin = {
  form: {
    username: '',
    password: '',
  },
};

const LoginReducer = (state = initialStateLogin, action) => {
  if (action.type === 'SET_FORM') {
    return {
      ...state,
      form: {
        ...state.form,
        [action.inputType]: action.inputValue,
      },
    };
  }
  return state;
};

const reducer = combineReducers({
  LoginReducer,
});

export default reducer;
