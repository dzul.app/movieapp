import React from 'react';
import {View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import SplashScreen from '../screen/Splash/Splash';
import LoginScreen from '../screen/Login/Login';
import MainNavigator from '../navigations/MainNavigatior';

const RouterStack = createStackNavigator();
const Router = () => {
  <NavigationContainer>
    <RouterStack.Navigator initialRouteName="Splash">
      <RouterStack.Screen name="Splash" component={SplashScreen} />
      <RouterStack.Screen name="Login" component={LoginScreen} />
      <RouterStack.Screen name="Main" component={MainNavigator} />
    </RouterStack.Navigator>
  </NavigationContainer>;
};

export default Router;
