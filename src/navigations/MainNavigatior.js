import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../screen/Home/Home';
import DetailScreen from '../screen/Detail/Detail';
import AboutScreen from '../screen/About/About';

const MainStack = createStackNavigator();
const MainNavigator = () => {
  return (
    <NavigationContainer>
      <MainStack.Navigator>
        <MainStack.Screen
          name="Home"
          component={HomeScreen}
          options={{headerShown: false}}
        />
        <MainStack.Screen name="Detail" component={DetailScreen} />
        <MainStack.Screen name="About" component={AboutScreen} />
      </MainStack.Navigator>
    </NavigationContainer>
  );
};

export default MainNavigator;
